package br.com.livraria.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import br.com.livraria.dao.DAO;
import br.com.livraria.modelo.Autor;
import br.com.livraria.modelo.Livro;

@ManagedBean(name = "livroBean")
@ViewScoped
public class LivroBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Livro livro = new Livro();

    private Integer autorId;

    public void setAutorId(Integer autorId) {
	this.autorId = autorId;
    }

    public Integer getAutorId() {
	return autorId;
    }

    public Livro getLivro() {
	return livro;
    }

    public List<Livro> getLivros() {
	return new DAO<Livro>(Livro.class).listaTodos();
    }

    public List<Autor> getAutores() {
	return new DAO<Autor>(Autor.class).listaTodos();
    }

    public List<Autor> getAutoresDoLivro() {
	return this.livro.getAutores();
    }

    public String gravarAutor() {
	Autor autor = new DAO<Autor>(Autor.class).buscaPorId(this.autorId);
	this.livro.adicionaAutor(autor);
	System.out.println("Escrito por: " + autor.getNome());

	return "";
    }

    public String gravar() {
	System.out.println("Gravando livro " + this.livro.getTitulo());

	if (livro.getAutores().isEmpty()) {
	    FacesContext.getCurrentInstance().addMessage("autor",
		    new FacesMessage("Livro deve ter pelo menos um Autor."));
	    return "";
	}

	new DAO<Livro>(Livro.class).adiciona(this.livro);

	this.livro = new Livro();

	return "";
    }

    public String formAutor() {
	System.out.println("Chamanda do formulário do Autor.");
	return "autor?faces-redirect=true";
    }

    public void comecaComDigitoUm(FacesContext fc, UIComponent component,
	    Object value) throws ValidatorException {

	String valor = value.toString();
	if (!valor.startsWith("1")) {
	    throw new ValidatorException(new FacesMessage(
		    "ISBN deveria começar com 1"));
	}

    }
}
